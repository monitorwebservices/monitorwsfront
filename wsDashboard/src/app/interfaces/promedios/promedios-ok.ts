export interface PromediosOk {
  aplicativo_responsable: string;
  interfaz: string;
  servicio: string;
  metodo: string;
  total_invocaciones: number;
  total_ejcuciones_ok: number;
  tiempo_maximo: number;
  tiempo_minimo: number;
  tiempo_promedio: number;
}
