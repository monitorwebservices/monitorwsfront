export interface Top10errores {
  interfaz: string;
  canal: string;
  servicio: string;
  metodo: string;
  total: number;
}
