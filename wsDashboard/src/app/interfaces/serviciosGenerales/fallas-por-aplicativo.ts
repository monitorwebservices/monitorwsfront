export interface FallasPorAplicativo {
  servicio: string;
  metodo: string;
  responsable: string;
  total: number;
}
