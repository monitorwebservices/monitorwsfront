export interface Top10TiemposRespuesta {
  servicio: string;
  responsable: string;
  tiempo_respuesta: number;
}
