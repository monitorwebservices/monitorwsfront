import { RouterModule, Routes } from '@angular/router';
import { TopServiciosComponent } from './components/topServicios/top-servicios.component';
import { HistoricoComponent } from './components/historicos/historico.component';

const APP_ROUTES: Routes = [
  { path: 'topServicios', component: TopServiciosComponent },
  { path: 'historico', component: HistoricoComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'topServicios' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash : true});
