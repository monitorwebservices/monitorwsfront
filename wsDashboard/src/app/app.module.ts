import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { ChartsModule } from 'ng2-charts';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import localeMx from '@angular/common/locales/es-MX';

 // registrar los locales con el nombre que quieras utilizar a la hora de proveer
registerLocaleData(localeMx, 'es');

//Rutas
import { APP_ROUTING } from './app.routes';

import { ServicioPorInterfazService } from './servicios/top-ten-interfaz.service';
import { TopTenErroresPorInterfazService } from './servicios/top-ten-errores.service';

/*COMPONENTES*/
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { TopServiciosComponent } from './components/topServicios/top-servicios.component';
import { HistoricoComponent } from './components/historicos/historico.component';

/*SERVICIOS*/
/*Servicios CASH*/
import { Top10ConsumosService } from './servicios/serviciosCash/top-10-consumos-service';
import { Top10FallasService } from './servicios/serviciosCash/top-10-fallas-service';
import { Top10TiemposRespuestaService } from './servicios/serviciosCash/top-10-tiempos-respuesta-service';
/*Servicios Generales*/
import { ErroresPorCanalService } from './servicios/serviciosGenerales/errores-por-canal.service';
import { TiemposRespuestaCanalService } from './servicios/serviciosGenerales/tiempos-respuesta-canal.service';
import { ErroresPorAplicativoService } from './servicios/serviciosGenerales/errores-por-aplicativo.service';
/*Promedios*/
import { PromediosOkService } from './servicios/promedios/promedios-ok.service';

// Gráficos

/*Servicios Generales*/
import { ErroresPorCanalComponent } from './components/graficas/serviciosGenerales/errores-por-canal/errores-por-canal.component';
import { ErroresPorAplicativoComponent } from './components/graficas/serviciosGenerales/erroresPorAplicativo/errores-por-aplicativo.component';
import { TiemposRespuestaCanalComponent } from './components/graficas/serviciosGenerales/tiempos-respuesta-canal/tiempos-respuesta-canal.component';

import { FallasPorInterfazComponent } from './components/graficas/fallas-por-interfaz/fallas-por-interfaz.component';


import { PromediosComponent } from './components/promedios/promedios.component';
import { ConsumosComponent } from './components/graficas/serviciosCash/consumos/consumos.component';
import { TiemposDeRespuestaComponent } from './components/graficas/serviciosCash/tiempos-de-respuesta/tiempos-de-respuesta.component';
import { ErroresComponent } from './components/graficas/serviciosCash/errores/errores.component';
import { MainNavComponent } from './components/shared/sidebar/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatIconModule, MatListModule } from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    TopServiciosComponent,
    HistoricoComponent,
    FallasPorInterfazComponent,
    ErroresPorAplicativoComponent,
    ErroresPorCanalComponent,
    TiemposRespuestaCanalComponent,
    PromediosComponent,
    ConsumosComponent,
    TiemposDeRespuestaComponent,
    ErroresComponent,
    MainNavComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    ChartsModule,
    APP_ROUTING,
    MatSidenavModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatListModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es' },

    /*Servicios CASH*/
    Top10ConsumosService,
    Top10FallasService,
    Top10TiemposRespuestaService,
    /*Servicios Generales*/
    ErroresPorAplicativoService,
    ErroresPorCanalService,
    TiemposRespuestaCanalService,
    /*Promedios*/
    PromediosOkService,

    ServicioPorInterfazService,
    TopTenErroresPorInterfazService,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
