import { Injectable } from '@angular/core';
import { Http} from '@angular/http';
import { ActivatedRoute } from "@angular/router";
import 'rxjs/Rx';

@Injectable()
export class TopTenErroresPorInterfazService {
  ejecXInter = 'http://localhost:8080/logErrorController/ErroresPorInterfaz';



  constructor( private http: Http ) { }

   getErroresPorInterfaz() {
    return this.http.get( this.ejecXInter)
      .map(res => res.json());
  }

}
