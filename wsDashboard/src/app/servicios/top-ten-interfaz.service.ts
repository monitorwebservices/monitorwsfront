import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class ServicioPorInterfazService {
  ejecXInter = 'http://localhost:8080/logErrorController/top10Errores';

  constructor( private http: Http ) { }

   getData() {
    return this.http.get( this.ejecXInter)
      .map(res => res.json());
  }

}
