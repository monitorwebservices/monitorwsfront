import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute } from "@angular/router";

import 'rxjs/Rx';

@Injectable()
export class PromediosOkService {

 promediosOk = 'http://localhost:8080/monitorPromedios/PromediosNok';
 entorno: String;

  constructor(
    private http: Http,
    private route: ActivatedRoute
     ) {
      this.entorno = "default";
     }

  getPromediosOk() {

    this.route.queryParams
      .filter(params => params.entorno)
      .subscribe(params => {
        this.entorno = params.entorno;
      });

      let url = `${this.promediosOk}?entorno=${this.entorno}`;

    return this.http.get( url )
      .map(res => res.json());
  }
}
