import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {ActivatedRoute } from "@angular/router";
import 'rxjs/Rx';

@Injectable()
export class ErroresPorAplicativoService {
  erroresPorAplicativo = 'http://localhost:8080/monitorServiciosGeneral/ErroresPorAplicativo';
  entorno: String;

  constructor( private http: Http, private route: ActivatedRoute ) {
    this.entorno = "default";
   }

   getServiciosGeneralesErroresPorAplicativo() {

    this.route.queryParams
      .filter(params => params.entorno)
      .subscribe(params => {
        this.entorno = params.entorno;
      });

      let url = `${this.erroresPorAplicativo}?entorno=${this.entorno}`;

    return this.http.get( url )
      .map(res => res.json());
  }
}
