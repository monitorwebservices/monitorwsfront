import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {ActivatedRoute } from "@angular/router";

@Injectable()
export class ErroresPorCanalService {
  erroresPorCanal = 'http://localhost:8080/monitorServiciosGeneral/ErroresPorCanal';
  entorno: String;

  constructor( private http: Http, private route: ActivatedRoute  ) {
    this.entorno = "default";
  }

   getServiciosGeneralesErroresPorCanal() {

    this.route.queryParams
      .filter(params => params.entorno)
      .subscribe(params => {
        this.entorno = params.entorno;
      });

      let url = `${this.erroresPorCanal}?entorno=${this.entorno}`;

    return this.http.get( url )
      .map(res => res.json());
  }
}
