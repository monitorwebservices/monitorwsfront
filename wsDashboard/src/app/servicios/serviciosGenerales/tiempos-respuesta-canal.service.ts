import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {ActivatedRoute } from "@angular/router";

@Injectable()
export class TiemposRespuestaCanalService {
  tiemposRespuestaCanal = 'http://localhost:8080/monitorServiciosGeneral/TiemposRespuestaCanal';
  entorno: String;

  constructor( private http: Http, private route: ActivatedRoute   ) {
    this.entorno = "default";
  }

  getServiciosGeneralesTiemposRespuestaCanal() {

    this.route.queryParams
      .filter(params => params.entorno)
      .subscribe(params => {
        this.entorno = params.entorno;
      });

      let url = `${this.tiemposRespuestaCanal}?entorno=${this.entorno}`;

    return this.http.get( url )
      .map(res => res.json());
  }
}
