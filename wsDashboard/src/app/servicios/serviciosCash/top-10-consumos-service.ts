import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/Rx';

@Injectable()
export class Top10ConsumosService {
  top_10_consumos = 'http://localhost:8080/monitorCash/Consumos';
  entorno: String;

  constructor(
    private http: Http,
    private route: ActivatedRoute ) {
      this.entorno = "default";
   }

  getTop10Consumos() {

    this.route.queryParams
      .filter(params => params.entorno)
      .subscribe(params => {
        this.entorno = params.entorno;
      });

      let url = `${this.top_10_consumos}?entorno=${this.entorno}`;

    return this.http.get( url )
      .map(res => res.json());
  }

}
