import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";
import 'rxjs/Rx';

@Injectable()
export class Top10FallasService {
  top_10_fallas = 'http://localhost:8080/monitorCash/Fallas';
  entorno: String;

  constructor(
    private http: Http,
    private route: ActivatedRoute) {
      this.entorno = "default";
     }

  getTop10Fallas() {

    this.route.queryParams
      .filter(params => params.entorno)
      .subscribe(params => {
        this.entorno = params.entorno;
      });

      let url = `${this.top_10_fallas}?entorno=${this.entorno}`;

    return this.http.get( url )
      .map(res => res.json());
  }
}
