import { ErroresPorAplicativoService } from './../../../../servicios/serviciosGenerales/errores-por-aplicativo.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-errores-por-aplicativo',
  templateUrl: './errores-por-aplicativo.component.html'
})
export class ErroresPorAplicativoComponent {

  //propiedades de la gráfica
  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = false;
  public barChartData: any;

  public barChartOptions: any = {
    scaleShowVerticalLines: true,
    responsive: true,
    scaleShowValues: true,

    scaleValuePaddingX: 10,
    scaleValuePaddingY: 10,

    legend: {position: 'bottom'},
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
          color: "black"
        }
      }],
      yAxes: [{
        gridLines: {
          display: false,
          color: "black"
        }
      }]
    },
    plugins: {
      datalabels: {
          display: true,
          align: 'center',
          anchor: 'center'
      }
    }
  };

  public barChartColors:Array<any> = [
    { // dark grey
      backgroundColor: '#0093D1',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
  ]

  aplicativo: number[] = [];
  total: number[] = [];

  ngOnInit(): void {
    this.consultarDatosDelServicio();
  }

  private consultarDatosDelServicio() {
    this._top10aplicativo.getServiciosGeneralesErroresPorAplicativo()
    .subscribe( data => {
        for (let indx in data) {
          this.barChartLabels.push(data [indx]['servicio']);
          this.total.push(data [indx]['tiempo_respuesta']);
        }
        this.barChartData = [
          {data: this.total, label: this.barChartLabels[0]}
        ];
    });
  }

  constructor(private _top10aplicativo: ErroresPorAplicativoService) {
    this.aplicativo = [];
    this.barChartData = [
      {data: [], label: ''}
    ];
  }


}
