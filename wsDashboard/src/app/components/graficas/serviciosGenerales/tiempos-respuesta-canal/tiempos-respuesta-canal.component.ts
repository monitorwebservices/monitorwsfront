import { Component, OnInit } from '@angular/core';
import { TiemposRespuestaCanalService } from 'src/app/servicios/serviciosGenerales/tiempos-respuesta-canal.service';

@Component({
  selector: 'app-tiempos-respuesta-canal',
  templateUrl: './tiempos-respuesta-canal.component.html'
})
export class TiemposRespuestaCanalComponent implements OnInit {

  //propiedades de la gráfica
  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = false;
  public barChartData: any;

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    legend: {position: 'bottom'},
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
          color: "black"
        }
      }],
      yAxes: [{
        gridLines: {
          display: false,
          color: "black"
        }
      }]
    },
    animation: {
      onComplete: function () {
          var chartInstance = this.chart,
          ctx = chartInstance.ctx;
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';
          this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function (bar, index) {
                  var data = dataset.data[index];
                  ctx.fillText(data, bar._model.x, bar._model.y - 5);
              });
          });
      }
     }
  };

  public barChartColors:Array<any> = [
    { // dark grey
      backgroundColor: '#0093D1',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }];

  tiemposRespuestaCanal: any[] = [];
  totales: number[] = [];

 ngOnInit(): void {
   this.consultarDatosDelServicio();
 }

 private consultarDatosDelServicio() {
  this._tiemposRespuestaCanal.getServiciosGeneralesTiemposRespuestaCanal()
  .subscribe( data => {
      for (let indx in data) {
        this.barChartLabels.push(data [indx]['interfaz']);
        this.totales.push(data [indx]['tiempoRespuesta']) ;
      }
      this.barChartData = [
        {data: this.totales, label: this.barChartLabels[0]}
      ];
  });
  }

  constructor(private _tiemposRespuestaCanal: TiemposRespuestaCanalService) {
    this.tiemposRespuestaCanal = [];
    this.barChartData = [
      {data: [], label: ''}
    ];
  }

}
