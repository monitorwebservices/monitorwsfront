
import { Datos } from './../../../interfaces/datos';
import { Component } from '@angular/core';
import { valoresPorInterfaz } from 'src/app/interfaces/valores-por-interfaz.interfaz';
import { TopTenErroresPorInterfazService } from 'src/app/servicios/top-ten-errores.service';

@Component({
  selector: 'app-fallas-por-interfaz',
  templateUrl: './fallas-por-interfaz.component.html'
})

export class FallasPorInterfazComponent{

  //propiedades de la gráfica
  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = false;
  public barChartData: any;


  public barChartOptions: any = {
    scaleShowVerticalLines: true,
    responsive: true,
    scaleShowValues: true,

    scaleValuePaddingX: 10,
    scaleValuePaddingY: 10,

    legend: {position: 'bottom'},
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
          color: "black"
        }
      }],
      yAxes: [{
        gridLines: {
          display: false,
          color: "black"
        }
      }]
    },
    plugins: {
      datalabels: {
          display: true,
          align: 'center',
          anchor: 'center'
      }
    }
  };

  public barChartColors:Array<any> = [
    { // dark grey
      backgroundColor: '#0093D1',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
  ]
  //erroresPorInter: valoresPorInterfaz[] = [];
  erroresPorInter: number[] = [];
  totales: number[] = [];

  ngOnInit(): void {
      this.consultarDatosDelServicio();
  }

  private consultarDatosDelServicio() {
    this._top10ErroresXInter.getErroresPorInterfaz()
    .subscribe( data => {
        for (let indx in data) {
          this.barChartLabels.push(data [indx]['interfaz']);
          this.totales.push(data [indx]['total']);
        }
        this.barChartData = [
          {data: this.totales, label: this.barChartLabels[0]}
        ];
    });

  }

  constructor(private _top10ErroresXInter:  TopTenErroresPorInterfazService) {
    this.erroresPorInter = [];
    this.barChartData = [
      {data: [], label: ''}
    ];
  }
}
