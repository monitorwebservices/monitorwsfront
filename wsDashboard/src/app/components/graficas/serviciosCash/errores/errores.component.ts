import { Component, OnInit } from '@angular/core';
import { Top10FallasService } from 'src/app/servicios/serviciosCash/top-10-fallas-service';

@Component({
  selector: 'app-errores',
  templateUrl: './errores.component.html'
})
export class ErroresComponent{

  //propiedades de la gráfica
  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = false;
  public barChartData: any;


  public barChartOptions: any = {
    scaleShowVerticalLines: true,
    responsive: true,
    scaleShowValues: true,

    scaleValuePaddingX: 10,
    scaleValuePaddingY: 10,

    legend: {position: 'bottom'},
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
          color: "black"
        }
      }],
      yAxes: [{
        gridLines: {
          display: false,
          color: "black"
        }
      }]
    },
    plugins: {
      datalabels: {
          display: true,
          align: 'center',
          anchor: 'center'
      }
    },
    animation: {
      onComplete: function () {
          var chartInstance = this.chart,
          ctx = chartInstance.ctx;
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function (bar, index) {
                  var data = dataset.data[index];
                  ctx.fillText(data, bar._model.x, bar._model.y - 1);
              });
          });
      }
     }
  };

  public barChartColors:Array<any> = [
    { // dark grey
      backgroundColor: '#0093D1',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
  ]

  servicio: number[] = [];
  total: number[] = [];

  ngOnInit(): void {
    this.consultarDatosDelServicio();
  }

  private consultarDatosDelServicio() {
    this._top10ErroresCash.getTop10Fallas()
    .subscribe( data => {
        for (let indx in data) {
          this.barChartLabels.push(data [indx]['servicio']);
          this.total.push(data [indx]['total']);
        }
        this.barChartData = [
          {data: this.total, label: this.barChartLabels[0]}
        ];
    });
  }

  constructor(private _top10ErroresCash: Top10FallasService) {
    this.servicio = [];
    this.barChartData = [
      {data: [], label: ''}
    ];
  }

}
