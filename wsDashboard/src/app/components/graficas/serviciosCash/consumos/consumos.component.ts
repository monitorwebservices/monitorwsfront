import { Top10ConsumosService } from './../../../../servicios/serviciosCash/top-10-consumos-service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-consumos',
  templateUrl: './consumos.component.html'
})
export class ConsumosComponent {

  //propiedades de la gráfica
  public barChartLabels: string[] = [];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = false;
  public barChartData: any;


  public barChartOptions: any = {
    scaleShowVerticalLines: true,
    responsive: true,
    scaleShowValues: true,

    scaleValuePaddingX: 10,
    scaleValuePaddingY: 10,

    legend: {position: 'bottom'},
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
          color: "black"
        }
      }],
      yAxes: [{
        gridLines: {
          display: false,
          color: "black"
        },
        scaleLabel: {
          display: false,
      },
      }]
    },
    animation: {
      onComplete: function () {
          var chartInstance = this.chart,
          ctx = chartInstance.ctx;
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function (bar, index) {
                  var data = dataset.data[index];
                  ctx.fillText(data, bar._model.x, bar._model.y - 1);
              });
          });
      }
     }
  };

  public barChartColors:Array<any> = [
    { // dark grey
      backgroundColor: '#0093D1',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
  ]

  consumos: number[] = [];
  totales: number[] = [];

  ngOnInit(): void {
    this.consultarDatosDelServicio();
  }

  private consultarDatosDelServicio() {
    this._top10Consumos.getTop10Consumos()
    .subscribe( data => {
        for (let indx in data) {
          this.barChartLabels.push(data [indx]['servicio']);
          this.totales.push(data [indx]['total']);
        }
        this.barChartData = [
          {data: this.totales, label: this.barChartLabels[0]}
        ];
    });
  }

  constructor(private _top10Consumos: Top10ConsumosService) {

    this.consumos = [];
    this.barChartData = [
      {data: [], label: ''}
    ];
  }
}
