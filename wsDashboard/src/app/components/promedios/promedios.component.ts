import { Component, OnInit } from '@angular/core';
import { PromediosOk } from 'src/app/interfaces/promedios/promedios-ok';
import { PromediosOkService } from 'src/app/servicios/promedios/promedios-ok.service';

@Component({
  selector: 'app-promedios',
  templateUrl: './promedios.component.html'
})
export class PromediosComponent implements OnInit{

  public registroPromedios: PromediosOk[] = [];

  ngOnInit(): void {
    this.consultarDatosDelServicio();
  }

  private consultarDatosDelServicio() {
    this._promediosOk.getPromediosOk()
    .subscribe( data => {
      for (let indx in data) {
        console.log(data [indx])
        this.registroPromedios.push(data [indx]);
      }
    });

  }

  constructor(private _promediosOk:  PromediosOkService) {
      this.registroPromedios = [];
   }
}
