import { Component, OnInit, Input } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { ActivatedRoute } from "@angular/router";
import 'rxjs/add/operator/filter';

/*SERVICIOS CASH*/
import { Top10Consumos } from './../../interfaces/serviciosCash/top-10-consumos';

import { Top10errores } from './../../interfaces/top10errores';
import { ServicioPorInterfazService } from 'src/app/servicios/top-ten-interfaz.service';
import { TopTenErroresPorInterfazService } from 'src/app/servicios/top-ten-errores.service';
import { Top10ConsumosService } from 'src/app/servicios/serviciosCash/top-10-consumos-service';

@Component({
  selector: 'app-top-servicios',
  templateUrl: './top-servicios.component.html'
})
export class TopServiciosComponent implements OnInit {

  consumos: Top10Consumos[];
  servicioPorInter: Top10errores[] = [];
  erroresPorInter: any[] = [];
  entorno: string;
  order: string;


  //constructor(private _servicioXInter: ServicioPorInterfazService, private _top10ErroresXInter:  TopTenErroresPorInterfazService) {
  constructor(
    private _top10ConsumosService: Top10ConsumosService,
    private http: Http,
    private _route: ActivatedRoute
    ) {
      this.consumos = [];

      this._route.queryParams
      .filter(params => params.order)
      .subscribe(params => {
        console.log(params); // {order: "popular"}

        this.order = params.order;
        console.log(this.order); // popular
      });

    }

  ngOnInit(): void{
  }

}
